import axiosWord from "../../axios-word";

export const FETCH_WORD_REQUEST = 'FETCH_WORD_REQUEST';
export const FETCH_WORD_SUCCESS = 'FETCH_WORD_SUCCESS';
export const FETCH_WORD_FAILURE = 'FETCH_WORD_FAILURE';
export const CODED_WORD = 'CODED_WORD';
export const SET_PASSWORD = 'SET_PASSWORD';
export const DECODED_WORD = 'DECODED_WORD';

export const fetchWordRequest = () => ({type: FETCH_WORD_REQUEST});
export const fetchWordSuccess = () => ({type: FETCH_WORD_SUCCESS});
export const fetchWordFailure = error => ({type: FETCH_WORD_FAILURE, error});
export const codedWord = word => ({type: CODED_WORD, word});
export const setPassword = password => ({type: SET_PASSWORD, password});
export const decodedWord = word => ({type: DECODED_WORD, word});


export const codeWord = (message, action) => {
    return async dispatch => {
        dispatch(setPassword(message.password));
        try {
            dispatch(fetchWordRequest());
            if (action === 'encode'){
                dispatch(codedWord(message.message));
                const response = await axiosWord.post('/encode', message);
                dispatch(decodedWord(response.data))
            } else if( action ==='decode'){
                dispatch(decodedWord(message.message));
                const response = await axiosWord.post('/decode', message);
                dispatch(codedWord(response.data))
            }
            dispatch(fetchWordSuccess());
        } catch (error) {
            dispatch(fetchWordFailure(error));
        }
    };
};

