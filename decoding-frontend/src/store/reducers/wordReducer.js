import {
    CODED_WORD,
    DECODED_WORD,
    FETCH_WORD_FAILURE,
    FETCH_WORD_REQUEST,
    FETCH_WORD_SUCCESS, SET_PASSWORD
} from "../actions/wordActions";

const initialState = {
    fetchLoading: false,
    fetchError: false,
    decoded: '',
    password:'',
    encoded: ''
};

const wordReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_WORD_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_WORD_SUCCESS:
            return {...state, fetchLoading: false};
        case FETCH_WORD_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.error};
        case CODED_WORD:
            return {...state, decoded: action.word};
        case SET_PASSWORD:
            return {...state, password: action.password};
        case DECODED_WORD:
            return {...state, encoded: action.word}
        default:
            return state;
    }
};


export default wordReducer;