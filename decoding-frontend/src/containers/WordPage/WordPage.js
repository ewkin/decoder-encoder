import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import makeStyles from "@material-ui/core/styles/makeStyles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import {codeWord} from "../../store/actions/wordActions";

const useStyles = makeStyles((theme) => ({
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '25ch',
    },
}));

const WordPage = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const wordState = useSelector(state => state);

    const [words, setWords] = useState({
        code: '',
        password: '',
        decode: '',
    });

    useEffect(() => {
        setWords(prevState => ({
            ...prevState,
            code: wordState.decoded,
            password: wordState.password,
            decode: wordState.encoded,
        }))
    }, [wordState]);


    const wordsChanged = event => {
        const {name, value} = event.target;
        setWords(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const decodeWords = (event) => {
        event.preventDefault();
        if (words.code === '') {
            let message = {'password': words.password, 'message': words.decode};
            dispatch(codeWord(message, 'decode'))
        } else {
            let message = {password: words.password, message: words.code};
            dispatch(codeWord(message, 'encode'));
        }
    };


    return (
        <div>
            <form className={classes.root} noValidate onSubmit={decodeWords} autoComplete="off">
                <TextField
                    label="Decoded message"
                    onChange={wordsChanged}
                    name='code'
                    style={{margin: 8}}
                    value={words.code}
                    placeholder="Type a message"
                    fullWidth
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="outlined"
                />
                <TextField
                    label="Password"
                    value={words.password}
                    onChange={wordsChanged}
                    name='password'
                    placeholder="Type a password"
                    className={classes.textField}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="outlined"
                />
                <Button variant="contained" color="secondary" type="submit"
                        disabled={words.code === '' || words.password === ''}>
                    <ArrowDownwardIcon/>
                </Button>
                <Button variant="contained" color="secondary" type="submit"
                        disabled={words.decode === '' || words.password === ''}>
                    <ArrowUpwardIcon/>
                </Button>
                <TextField
                    label="Encoded message"
                    style={{margin: 8}}
                    onChange={wordsChanged}
                    name='decode'
                    value={words.decode}
                    placeholder="Type a message"
                    fullWidth
                    margin="normal"
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="outlined"
                />
            </form>
        </div>
    );
};

export default WordPage;