import React from 'react';
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import WordPage from "./containers/WordPage/WordPage";

const App = () => (
  <>
      <CssBaseline/>
      <main>
          <Container maxWidth="xl">
              <WordPage/>
          </Container>
      </main>
  </>
);

export default App;
