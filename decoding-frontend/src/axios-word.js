import axios from 'axios';

const axiosWord = axios.create({
    baseURL: 'http://localhost:8000'
});

export default axiosWord;