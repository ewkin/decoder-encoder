const express = require('express');
const app = express();

const Caesar = require('caesar-salad').Caesar;
const cors = require('cors');

app.use(express.json());
app.use(cors());

const port = 8000;

app.post('/encode', (req, res) => {
    let message = req.body;
    res.send(Caesar.Cipher(message.password).crypt(message.message));
});

app.post('/decode', (req, res) => {
    let message = req.body;
    res.send(Caesar.Decipher(message.password).crypt(message.message));
});

app.listen(port, () => {
    console.log('We are live on ' + port);
})